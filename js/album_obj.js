function Album(aName, aYear, aDesc, aCover) {
	this.name=aName;
	this.year=aYear;
	this.desc=aDesc;
	this.cover=aCover;
	
	//вывод в таблицу
	this.displayShortInfo=function(){
		
		document.write("<tr><td>"+this.year+"</td><td>"+this.name+"</td></tr>");
	};
	
	//то же самое, но через DOM
	this.displayShortInfoDOM=function(){
		
		var discographyElement=document.getElementById("discog");
		var discographyTableElement=discographyElement.getElementsByTagName("table")[0];

		var newRow=document.createElement("tr");

		var newYear=document.createElement("td");
		var newName=document.createElement("td");
		
		newYear.appendChild(document.createTextNode(this.year));
		newName.appendChild(document.createTextNode(this.name));
		
		newRow.appendChild(newYear);
		newRow.appendChild(newName);

		discographyTableElement.appendChild(newRow);
	};
	this.displayFullInfo=function(){
		
		document.write("<h1>"+this.name+" ("+this.year+") "+"</h1>");
		document.write("<p>"+this.desc+"</p>");
	};
	this.displayFullInfoDOM=function(){

		var albumElement=document.getElementById("album");
		
		var newImgBlock=document.createElement("div");
		newImgBlock.setAttribute("id", "albumcover");
		var newImg=document.createElement("img");
		newImg.setAttribute("src", this.cover);
		newImgBlock.appendChild(newImg);
		albumElement.appendChild(newImgBlock);
		
		var newHeader=document.createElement("h1");
		newHeader.appendChild(document.createTextNode(this.name+" ("+this.year+") "));
		albumElement.appendChild(newHeader);

		var newDesc=document.createElement("p");
		newDesc.appendChild(document.createTextNode(this.desc));
		albumElement.appendChild(newDesc);

	};
}